// Service darksky
var https = require('https');
var http = require('http');
var body = '';
const cors = require('cors');
var options = {
    host: 'api.darksky.net',
    port: 443,
    path: '/forecast/6c44e123a6150d9b74a828772000908d/37.8267,-122.4233'
};
https.get(options,function(res){
    
    res.on('data',function(chunk){
        body+=chunk;
        //console.log(chunk);
    });
    res.on('end',function(){
        var hour = JSON.parse(body);
        console.log(hour);
        return hour;
    })
}
).end()

http.createServer(function(request, response) {
    const headers = {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'OPTIONS, POST, GET',
        'Access-Control-Max-Age': 2592000, // 30 days
        /** add other headers too */
      };
    console.log("Conectado al puerto 8888");
    response.writeHead(200, headers);
    response.write(body);
    response.end();
    
  }).listen(8888);

