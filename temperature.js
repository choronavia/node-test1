const express = require('express');
const app = express();
const cors = require('cors');

app.use(express.static(__dirname + '/public/'));
app.use(cors());
var options = {
    host: 'api.darksky.net',
    port: 443,
    path: '/forecast/6c44e123a6150d9b74a828772000908d/37.8267,-122.4233'
};
app.get('/temperature', function (req, res) {
    var data = {
      "bestAnimals": [
        "wombat",
        "corgi",
        "puffer fish",
        "owl",
        "crow"
      ]
    };
  
    res.json(data);
  });
  
  
  var server = app.listen(3000, function () {
  
    var host = server.address().address;
    var port = server.address().port;
  
    console.log('Example app listening at http://%s:%s', host, port);
  
  });